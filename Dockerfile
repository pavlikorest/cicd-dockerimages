FROM python:3.6-alpine
RUN apk --no-cache add postgresql-client
WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt
COPY . /usr/src/app
CMD ["python", "app.py"]
